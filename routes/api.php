<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\ChatController;
use App\Http\Controllers\Api\V1\ChatNetworkController;
use App\Http\Controllers\Api\V1\GroupChatController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["middleware" => "api", "prefix" => "v1"], function () {
    Route::post("/register", [AuthController::class, "register"]);
    Route::post("/login", [AuthController::class, "login"]);


    Route::group(["middleware" => "jwt.verify"], function () {
        Route::post("/logout", [AuthController::class, "logout"]);
        Route::put("/edit-profile", [UserController::class, "update"]);
        Route::get("/get-user-by-token", [AuthController::class, "me"]);

        // Group Chat routes
        Route::get('group-chat', [GroupChatController::class, 'index']);
        Route::get('group-chat/{id}/pendings', [GroupChatController::class, 'pendings']);
        Route::post('group-chat', [GroupChatController::class, 'store']);
        Route::get('group-chat/{id}', [GroupChatController::class, 'show'])->where('id', '[0-9]+');
        Route::put('group-chat/{id}', [GroupChatController::class, 'update'])->where('id', '[0-9]+');
        Route::delete('group-chat/{id}', [GroupChatController::class, 'destroy'])->where('id', '[0-9]+');

        // Chat Network routes
        Route::get('chat-networks', [ChatNetworkController::class, 'index']);
        Route::post('chat-networks', [ChatNetworkController::class, 'store']);
        Route::get('chat-networks/{id}', [ChatNetworkController::class, 'show'])->where('id', '[0-9]+');
        Route::put('chat-networks/{id}', [ChatNetworkController::class, 'update'])->where('id', '[0-9]+');
        Route::delete('chat-networks/{id}', [ChatNetworkController::class, 'destroy'])->where('id', '[0-9]+');

        // Chat routes
        Route::get('chats', [ChatController::class, 'index']);
        Route::post('chats', [ChatController::class, 'store']);
        Route::put('chats/{id}', [ChatController::class, 'update'])->where('id', '[0-9]+');
        Route::delete('chats/{id}', [ChatController::class, 'destroy'])->where('id', '[0-9]+');
    });

});