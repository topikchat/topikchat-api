<?php
// app/Library/Helpers.php

namespace App\Library;

use App\Models\User;

class HelperLib
{

    public static function getValidatorErrorMessages($validator)
    {
        $message = implode("\n", $validator->errors()->all());
        return $message;
    }

    public static function getUser($param)
    {
        $user = User::where("id", $param)
            ->orWhere("email", $param)
            ->first();
        return $user;
    }

}