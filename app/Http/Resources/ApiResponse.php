<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApiResponse extends JsonResource
{
    public $status;
    public $message;
    public $newToken;

    public function __construct($status, $message, $resource, $newToken = null)
    {
        parent::__construct($resource);
        $this->status = $status;
        $this->message = $message;
        $this->newToken = $newToken;
    }

    public function toResponse($request)
    {
        return parent::toResponse($request)->setStatusCode($this->status);
    }

    public function toArray($request)
    {
        $response = [
            'status' => $this->status,
            'message' => $this->message,
            'data' => $this->resource
        ];
        if ($this->newToken != null) {
            $response['newToken'] = $this->newToken;
        }
        return $response;
    }
}