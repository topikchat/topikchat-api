<?php

namespace App\Http\Controllers\api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateChatRequest;
use App\Http\Requests\UpdateChatRequest;
use App\Http\Resources\ApiResponse;
use App\Services\ChatService;
use App\Services\GroupChatService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kreait\Laravel\Firebase\Facades\Firebase;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;


class ChatController extends Controller
{
    protected $service;
    protected $firebase;
    protected $messaging;

    public function __construct(ChatService $service)
    {
        $this->firebase = Firebase::project();
        $this->messaging = Firebase::messaging();
        $this->service = $service;
    }

    /**
     * @LRDparam group_id string
     * @lrd:start
     * Mendapatkan daftar semua chat.
     * Pastikan menggunakan query parameter group_id. Contoh: ?group_id=1
     * @lrd:end
     */
    public function index(Request $request)
    {
        $groupId = $request->query("group_id");
        $data = $this->service->getAllChats($groupId);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan semua data chat', $data);
    }

    public function sendNotification($groupId, $message, $media = null, $isDelete = false){
        $user = auth()->user();
        $gcService = new GroupChatService();
        $data = array(
            "id" => 0,
            "user_id" => $user['id'],
            "group_id" => $groupId, 
            "message" => $message,
            "media_link" => $media,
            "sent_at" => date("Y-m-d H:i:s"),
            "created_at" => date("Y-m-d H:i:s"), 
            "updated_at" => date("Y-m-d H:i:s"), 
            "user" => $user,
            "group" => $gcService->getGroupChatById($groupId),
            "is_delete" => $isDelete,
        );
        $message = CloudMessage::new()->withTarget("topic", "chat")
            ->withNotification(Notification::create('Pesan Baru', 'Pesan diterima'))
            ->withData($data);
        
        $this->messaging->send($message);
    }

    /**
     * @lrd:start
     * Membuat chat baru.
     * @lrd:end
     */
    public function store(CreateChatRequest $request)
    {
        $data = $request->validated();
        $this->sendNotification($data['group_id'], $data['message']);
        $this->service->createChat($data);
        return new ApiResponse(Response::HTTP_CREATED, 'Berhasil membuat chat', null);
    }
    /**
     * @lrd:start
     * Memperbarui chat yang sudah ada.
     * @lrd:end
     */
    public function update(UpdateChatRequest $request, $id)
    {
        $data = $request->validated();
        $this->sendNotification($data['group_id'], $data['message']);
        $this->service->updateChat($id, $data);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil merubah chat', null);
    }

    /**
     * @lrd:start
     * Menghapus chat.
     * @lrd:end
     */
    public function destroy($id)
    {
        $this->sendNotification($id, "", null, true);
        $this->service->deleteChat($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil menghapus chat', null);
    }
}