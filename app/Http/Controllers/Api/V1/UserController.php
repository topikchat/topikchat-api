<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditUserRequest;
use App\Http\Resources\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\UserService;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @lrd:start
     * Mengambil daftar pengguna.
     * @lrd:end
     */
    public function index()
    {
        //
    }

    /**
     * @lrd:start
     * Membuat pengguna baru.
     * @lrd:end
     */
    public function store(Request $request)
    {

    }

    /**
     * @lrd:start
     * Menampilkan detail pengguna berdasarkan ID.
     * @lrd:end
     */
    public function show($id)
    {
        //
    }

    /**
     * @lrd:start
     * Mengedit profil pengguna berdasarkan ID.
     * @lrd:end
     */
    public function update(EditUserRequest $request)
    {
        $data = $request->validated();
        $user = $this->userService->editProfile($data);
        return new ApiResponse(Response::HTTP_OK, "Berhasil merubah akun", $user);
    }

    /**
     * @LRDparam user_id string
     * @lrd:start
     * Menghapus pengguna berdasarkan ID.
     * Pastikan menggunakan query parameter user_id. Contoh: ?user_id=1
     * @lrd:end
     */
    public function destroy($id)
    {
        //
    }
}