<?php

namespace App\Http\Controllers\api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateChatNetworkRequest;
use App\Http\Requests\UpdateChatNetworkRequest;
use App\Http\Resources\ApiResponse;
use App\Models\GroupChat;
use App\Services\ChatNetworkService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ChatNetworkController extends Controller
{
    protected $service;

    public function __construct(ChatNetworkService $service)
    {
        $this->service = $service;
    }

    /**
     * @lrd:start
     * Mendapatkan daftar semua Chat Network.
     * Pastikan menggunakan query parameter user_id. Contoh: ?user_id=1
     * @lrd:end
     *
     * @LRDparam user_id string
     */
    public function index(Request $request)
    {
        $userId = $request->query("user_id");
        $data = $this->service->getAllChatNetworks($userId);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan semua data Chat Network', $data);
    }

    /**
     * @lrd:start
     * Gabung ke grup.
     * @lrd:end
     */
    public function store(CreateChatNetworkRequest $request)
    {
        $data = $request->validated();
        $message = $this->service->createChatNetwork($data);

        return new ApiResponse(Response::HTTP_CREATED, $message, null);
    }

    /**
     * @lrd:start
     * Mendapatkan informasi detail tentang Chat Network tertentu.
     * @lrd:end
     */
    public function show($id)
    {
        $data = $this->service->getChatNetworkById($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan semua data Chat Network', $data);
    }

    /**
     * @lrd:start
     * Memperbarui Chat Network / Keanggotaan user dalam grup.
     * @lrd:end
     */
    public function update(UpdateChatNetworkRequest $request, $id)
    {
        $data = $request->validated();
        $this->service->updateChatNetwork($id, $data);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil merubah Chat Network', null);
    }

    /**
     * @lrd:start
     * Keluar dari grup.
     * @lrd:end
     */
    public function destroy($id)
    {
        $this->service->deleteChatNetwork($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil menghapus Chat Network', null);
    }
}