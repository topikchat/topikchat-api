<?php

namespace App\Http\Controllers\api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Http\Resources\ApiResponse;
use App\Services\GroupChatService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GroupChatController extends Controller
{
    protected $service;

    public function __construct(GroupChatService $service)
    {
        $this->service = $service;
    }

    /**
     * @lrd:start
     * Mendapatkan semua data group chat yang aktif.
     * @lrd:end
     */
    public function index()
    {
        $data = $this->service->getAllGroupChats();
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan semua data group chat', $data);
    }

    public function pendings($id)
    {
        $data = $this->service->getAllRequestPendings($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan semua data request pending', $data);
    }

    /**
     * @lrd:start
     * Membuat grup chat baru.
     * @lrd:end
     */
    public function store(CreateGroupRequest $request)
    {
        $data = $request->validated();
        $returned = $this->service->createGroupChat($data);
        return new ApiResponse(Response::HTTP_CREATED, 'Berhasil membuat grup chat', $returned);
    }

    /**
     * Mendapatkan data group chat berdasarkan ID.
     */
    public function show(Request $request, $id)
    {
        $data = $this->service->getGroupChatById($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil mendapatkan data group chat', $data);
    }

    /**
     * @lrd:start
     * Mengupdate informasi grup chat.
     * @lrd:end
     */
    public function update(UpdateGroupRequest $request, $id)
    {
        $data = $request->validated();
        $this->service->updateGroupChat($id, $data);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil merubah grup chat', null);
    }

    /**
     * @lrd:start
     * Menghapus grup chat berdasarkan ID.
     * @lrd:end
     */
    public function destroy($id)
    {
        $this->service->deleteGroupChat($id);
        return new ApiResponse(Response::HTTP_OK, 'Berhasil menghapus grup chat', null);
    }
}