<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateChatRequest extends IndiffFormRequest
{


    public function rules()
    {
        return [
            "message" => "required|string",
        ];
    }
}