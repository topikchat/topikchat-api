<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateChatNetworkRequest extends IndiffFormRequest
{
    public function rules()
    {
        return [
            "group_id" => "required|integer",
            "user_id" => "required|integer",
        ];
    }
}