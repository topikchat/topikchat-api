<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateChatNetworkRequest extends IndiffFormRequest
{
    public function rules()
    {
        return [
            "left_at" => "nullable|date",
            "approved_at" => "nullable|date",
            "rating" => "nullable|integer",
        ];
    }
}