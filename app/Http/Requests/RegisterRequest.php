<?php

namespace App\Http\Requests;

class RegisterRequest extends IndiffFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:50',
            'email' => 'required|email',
            'username' => 'required|string|min:6|max:20',
            'password' => 'required|string|min:6',
        ];
    }
}