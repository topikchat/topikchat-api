<?php

namespace App\Http\Requests;


class CreateGroupRequest extends IndiffFormRequest
{

    public function rules()
    {
        return [
            "user_id" => "required|integer",
            "group_name" => "required|string|min:6",
            "description" => "required|string|min:6|max:150",
            "is_private" => "required|integer",
        ];
    }
}