<?php

namespace App\Http\Requests;


class UpdateGroupRequest extends IndiffFormRequest
{

    public function rules()
    {
        return [
            "group_name" => "string|min:6",
            "description" => "string|min:6|max:150",
            "is_active" => "integer",
            "is_private" => "integer",
        ];
    }
}