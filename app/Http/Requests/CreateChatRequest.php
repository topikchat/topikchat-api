<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateChatRequest extends IndiffFormRequest
{
    public function rules()
    {
        return [
            "group_id" => "required|integer",
            "user_id" => "required|integer",
            "message" => "required|string",
        ];
    }
}