<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Chat;
use App\Models\ChatNetwork;
use App\Models\GroupChat;
use Illuminate\Http\Response;



class ChatService
{
    //Mengirim chat, validasi dulu apakah grup nya aktif dan jika dia bergabung apa enggak di grup tersebut
    public function createChat(array $data)
    {
        $user = auth()->user();
        $groupExist = GroupChat::where("id", $data['group_id'])->where("is_active", 1)->first();
        if (!$groupExist) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup tidak ditemukan", null);
        }
        $dataChatNetwork = ChatNetwork::where("group_id", $data['group_id'])->where("user_id", $user->id)->first();
        if (!$dataChatNetwork) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Grup tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);
        }
        
        
        return Chat::create($data);
    }

    //Mendapatkan semua chat dari grup tertentu, validasi dulu apakah ada grupnya apa ga, dan apakah dia adalah member dari grup tersebut

    public function getAllChats($groupId)
    {
        if (!$groupId) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Harus disertai group_id pada query parameter!", null);
        }
        $groupExist = GroupChat::where("id", $groupId)->where("is_active", 1)->first();
        if (!$groupExist) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup tidak ditemukan", null);
        }

        $user = auth()->user();
        $dataChatNetwork = ChatNetwork::where("group_id", $groupId)->where("user_id", $user->id)->first();
        if (!$dataChatNetwork) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Data chat tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);
        }
        return Chat::with("user")->where("group_id", $groupId)->get();
    }

    //Update chat, validasi apakah dia bergabung apa engga, dan validasi apakah grup dan chatnya ada apa engga yang mau diedit
    public function updateChat(string $id, array $data)
    {
        $user = auth()->user();
        $record = Chat::with("user")->where("id", $id)->where("user_id", $user->id)->first();
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "Data chat tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);

        $groupExist = GroupChat::where("id", $record->group_id)->where("is_active", 1)->first();
        if (!$groupExist) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup tidak ditemukan", null);
        }

        $dataChatNetwork = ChatNetwork::where("group_id", $record->group_id)->where("user_id", $user->id)->first();
        if (!$dataChatNetwork) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Data chat tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);
        }
        $record->update($data);
        return $record->fresh();
    }

    /*Delete chat pada sebuah grup, validasi dulu apakah data exists, 
    apakah dia join pada grup tersebut, jika dia admin dia bisa delete, 
    jika dia bukan admin dia hanya bisa delete chat dia sendiri*/

    public function deleteChat(string $id)
    {
        $user = auth()->user();
        $record = Chat::where("id", $id)->first();
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "Data chat tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);

        $dataChatNetwork = ChatNetwork::with("group")->where("group_id", $record->group_id)->where("user_id", $user->id)->first();
        if (!$dataChatNetwork) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Data chat tidak ditemukan atau anda tidak bergabung pada grup tersebut!", null);
        }
        if ($dataChatNetwork->group->user_id != $user->id && $record->user_id != $user->id) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Data chat bukan milikmu dan kamu bukan admin!", null);
        }
        $record->destroy();
        return true;
    }
}