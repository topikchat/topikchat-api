<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\ChatNetwork;
use App\Models\GroupChat;
use Illuminate\Http\Response;

class ChatNetworkService
{
    //Join grup chat, validasi apakah grupnya ada, apakah dia sudah masuk grup atau tidak, jika private show diff messages
    public function createChatNetwork(array $data, $isOwner = false)
    {
        $groupData = GroupChat::where("id", $data['group_id'])->where("is_active", 1)->first();
        if (!$groupData) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup tidak ditemukan!", null);
        }

        $message = "";
        if ($groupData->is_private == 1 && !$isOwner) {
            $message = 'Berhasil mengirim request masuk grup. Silahkan ditunggu.';
        } else {
            $data['approved_at'] = now();
            $message = 'Berhasil masuk ke dalam grup. Jaga etika mu ya!';
        }

        // dd($data);
        $isExist = ChatNetwork::where("group_id", $data['group_id'])->where("user_id", $data['user_id'])->first();
        if ($isExist) {
            if ($isExist->left_at == null) {
                throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal menambah member! Anda sudah masuk/request ke grup ini", null);
            } else {
                $isExist->left_at = null;
                $isExist->approved_at = null;
                if ($groupData->is_private != 1) {
                    $isExist->approved_at = now();
                }
                $isExist->save();
                return $message;
            }
        }

        ChatNetwork::create($data);
        return $message;
    }

    //Mendapatkan semua grup chat yang user sudah join
    public function getAllChatNetworks($userId)
    {
        if ($userId) {
            return ChatNetwork::with("group")->where("user_id", $userId)->whereNull("left_at")->get();
        }
        return ChatNetwork::with("group")->get();
    }

    //Mendapatkan semua chat network by id, cek dlu jika grup nya aktif apa gak;

    public function getChatNetworkById(string $id)
    {
        $record = ChatNetwork::with("group")->find($id);
        if ($record->group->is_active == 0) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup tidak ditemukan", null);
        }
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "ChatNetwork dengan id: $id, tidak ditemukan!", null);
        return $record;
    }

    //Update approved, left_at, validasi apakah ada datanya dengan id chat network, dan yang melakukan update harus si admin dari grup
    public function updateChatNetwork(string $id, array $data)
    {
        $record = ChatNetwork::with("group")->find($id);
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "ChatNetwork dengan id: $id, tidak ditemukan!", null);

        $groupData = GroupChat::where("id", $record->group_id)->first();
        $user = auth()->user();
        if ($groupData->user_id != $user->id) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal merubah keanggotaan, anda bukan pemilik dari grup ini!", null);
        }

        if (array_key_exists('group_id', $data)) {
            unset($data['group_id']);
        }
        $record->update($data);
        return $record->fresh();
    }

    //Keluar grup chat, validasi apakah ada datanya, jika ada, ganti left_at biar tidak null
    public function deleteChatNetwork(string $id)
    {
        $record = ChatNetwork::find($id);
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "ChatNetwork dengan id: $id, tidak ditemukan!", null);

        $groupData = GroupChat::where("id", $record->group_id)->first();
        $user = auth()->user();
        if ($groupData->user_id != $user->id && $record->user_id != $user->id) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal merubah keanggotaan, anda bukan pemilik atau member dari grup ini!", null);
        }
        $record->update(["left_at" => now()]);
        return true;
    }
}