<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\ChatNetwork;
use App\Models\GroupChat;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Response;

class GroupChatService
{

    //User membuat grup chat
    public function createGroupChat(array $data)
    {

        $groupChat = GroupChat::create($data);
        $chatNetworkService = new ChatNetworkService();
        $newNetworkData = [
            "group_id" => $groupChat->id,
            "user_id" => $data['user_id']
        ];
        $chatNetworkService->createChatNetwork($newNetworkData, true);
        return $groupChat;
    }

    //User dapat mendapatkan semua grup chat yang aktif

    public function getAllGroupChats()
    {
        $data = GroupChat::with("members")->where("is_active", 1)->get();
        return $data;
    }

    /*Pemilik grup dapat melihat siapa 
    aja yang request untuk join grup dia*/

    public function getAllRequestPendings($id)
    {
        $user = auth()->user();
        $group = GroupChat::where("id", $id)->first();
        if ($group && $group->user_id != $user->id) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, "Anda bukan pemilik grup ini!", null);
        }
        $data = ChatNetwork::with(["user", "group"])->where("group_id", $id)->whereNull("approved_at")->get();
        return $data;
    }

    /*Mendapatkan grup chat berdasarkan id
    cek apakah data ada, cek apakah dia join grup tersebut
    jika ada maka tampilkan datanya jika engga tampilkan error
     */
    public function getGroupChatById(string $id)
    {
        $record = GroupChat::with("members")->where("id", $id)->where("is_active", 1)->first();
        if (!$record) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup chat dengan id: $id, tidak ditemukan!", null);
        }

        $user = auth()->user();
        $chatNetwork = ChatNetwork::where("user_id", $user->id)->where("group_id", $id)->whereNull("left_at")->first();
        if (!$chatNetwork) {
            // throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal mendapatkan data grup, anda bukan member grup tersebut!", null);
            $record->joined = false;
        } else if ($chatNetwork->approved_at == null) {
            $record->is_requesting = true;
        } else {
            $record->joined = true;
        }

        return $record;
    }

    /*Update grup chat, title, desc,
    validasi apakah grupchat aktif, 
    cek apakah yang update adalah pemilik dari grup,
    jika ia maka dia bisa edit, jika tidak ya error
     */
    public function updateGroupChat(string $id, array $data)
    {
        $record = GroupChat::with("members")->where("id", $id)->where("is_active", 1)->first();

        if (!$record) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Group chat dengan id: $id, tidak ditemukan!", null);
        }
        $user = auth()->user();
        if ($user->id != $record->user_id) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal merubah, karena anda bukan admin dari grup ini!", null);
        }

        $record->update($data);
        $record->load('members');

        return $record;
    }


    /*Delete group chat berdasarkan id,
    cek apakah data grup aktif, jika iya cek
    apakah yang mau delete adalah pemilik dari grup
    jika iya maka delete, jika tidak maka error
    */
    public function deleteGroupChat(string $id)
    {
        $record = GroupChat::with("members")->where("id", $id)->where("is_active", 1)->first();
        if (!$record)
            throw new ApiException(Response::HTTP_NOT_FOUND, "Grup chat dengan id: $id, tidak ditemukan!", null);

        $user = auth()->user();
        if ($user->id != $record->user_id) {
            throw new ApiException(Response::HTTP_NOT_FOUND, "Gagal merubah, karena anda bukan admin dari grup ini!", null);
        }
        $record->update(["is_active" => 0]);
        return true;
    }
}