<?php
namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\User;

class UserService
{
    public function editProfile(array $data)
    {

        $user = auth()->user();
        if (!$user)
            throw new ApiException(404, "Akun tidak ditemukan");

        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];

        $user->save();
        return $user;
    }
}