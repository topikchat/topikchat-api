<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $table = "chats";

    protected $fillable = [
        "user_id",
        "group_id",
        "message",
        "media_link",
    ];

    protected $casts = [
        'user_id' => 'integer',
        'group_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}