<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupChat extends Model
{
    use HasFactory;

    protected $table = "group_chats";

    protected $fillable = [
        'user_id',
        'group_name',
        'description',
        'is_active',
        'is_private',
    ];


    protected $casts = [
        'user_id' => 'integer',
        'is_active' => 'integer',
        'is_private' => 'integer',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function members()
    {
        return $this->hasMany(ChatNetwork::class, 'group_id')->whereNull("left_at");
    }

}