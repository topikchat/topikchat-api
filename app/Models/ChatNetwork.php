<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatNetwork extends Model
{
    use HasFactory;

    protected $table = "chat_networks";

    protected $fillable = [
        "group_id",
        "user_id",
        "rating",
        "approved_at",
        "left_at",

    ];

    protected $casts = [
        'user_id' => 'integer',
        'group_id' => 'integer',
        'rating' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function group()
    {
        return $this->belongsTo(GroupChat::class, "group_id");
    }


}